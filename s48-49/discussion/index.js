
fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => response.json())
	.then(data => showPosts(data))
	.catch(err => err)

const showPosts = (posts) => {
	let postEntries = ""
	for(let post of posts) {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">
					${post.title}
				</h3>
				<p id="post-body-${post.id}">
					${post.body}
				</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	}

	document.querySelector("#div-post-entries").innerHTML = postEntries
}

const showPost = async (id) => {
	const post = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
	const postData = await post.json()
	console.log(postData)
}

showPost(2)


document.querySelector("#form-add-post").addEventListener("submit", (event) => {
	event.preventDefault()
	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	}).then(response => response.json())
		.then(data => {
			console.log(data)
			alert("Post successfully added")
		})
		.catch(err => err)

	document.querySelector("#txt-title").innerHTML = null
	document.querySelector("#txt-body").innerHTML = null
})

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector("#txt-edit-id").value = id
	document.querySelector("#txt-edit-title").value = title
	document.querySelector("#txt-edit-body").value = body

	document.querySelector("#btn-submit-update").removeAttribute("disabled")
}

document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
	event.preventDefault()

	let id = document.querySelector("#txt-edit-id").value
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
		method: "PUT",
		body: JSON.stringify({
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	}).then(response => response.json())
		.then(data => {
			console.log(data)
			alert("Post updated")
		}).catch(err => err)

	document.querySelector("#txt-edit-title").value = null 
	document.querySelector("#txt-edit-body").value = null
	document.querySelector("#btn-submit-update").setAttribute("disabled", true)
})


const deletePost = async (id) => {
	const postToDelete = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: "DELETE",
		headers: {
			"Content-Type": "application/json"
		}
	}).then(response => {
		console.log(response)
		alert("post deleted")
		document.querySelector(`#post-${id}`).remove()
	})
	.catch(err => err)
}