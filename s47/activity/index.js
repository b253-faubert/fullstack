
const txtFirstName = document.getElementById("txt-first-name")
const txtLastName = document.getElementById("txt-last-name")
const spanFullName = document.getElementById('span-full-name')

function printFullName(event) {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
}

txtFirstName.addEventListener("keyup", printFullName)
txtLastName.addEventListener("keyup", printFullName)

