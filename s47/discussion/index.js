console.log(document.querySelector('#txt-first-name'))

console.log(document.getElementById("txt-first-name"))

const txtFirstName = document.getElementById("txt-first-name")
const txtLastName = document.getElementById("txt-last-name")
const spanFullName = document.getElementById('span-full-name')

txtFirstName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = txtFirstName.value
})

function printLastName(event) {
	spanFullName.innerHTML = txtLastName.value
}

txtLastName.addEventListener("keyup", printLastName)

txtFirstName.addEventListener("click", (event) => alert("clicked"))